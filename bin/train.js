const nlpController = require('../src/controllers/nlp');
const manager = require('../example.train.model');
const filename = 'example.model';

console.log('[!] Training....');
nlpController.trainModel(manager, filename);
console.log(`[!] Done, save as ${filename}`);
