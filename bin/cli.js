const prompts = require('prompts');
const {ConversationContext} = require('node-nlp');

const context = new ConversationContext();

const nlpController = require('../src/controllers/nlp');

const filename = 'example.model';
const lang = 'ru';

const createQ = async (message, cb) => {
  const response = await prompts({
    type: 'text',
    name: 'value',
    message,
  });
  return response;
};

// TODO: добавить ограничение точности
const process = async (res, manager) => {
  const {value} = await createQ(res.answer || '???');
  if (value === 'exit') {
    console.log('BB');
    return;
  }
  const result = await manager.process(lang, value, context);
  // console.log(result);
  // console.log("    ==    ");
  // console.log(context);
  await process(result, manager);
};

(async () => {
  console.log('[!] Loading model...');
  const manager = nlpController.loadModel(filename);
  console.log('[!] LOADED!');
  await process({answer: 'Say'}, manager);
})();

