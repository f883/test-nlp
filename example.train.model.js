const {NlpManager, ConversationContext} = require('node-nlp');

const manager = new NlpManager({languages: ['ru']});


// manager.addDocument('ru', 'Добрый день', 'greeting.hi');
// manager.addDocument('ru', 'Привет', 'greeting.hi');
// manager.addDocument('ru', 'Здравствуй', 'greeting.hi');
// manager.addDocument('ru', 'Хай', 'greeting.hi');
// manager.addDocument('ru', 'Дороу', 'greeting.hi');
//     manager.addAnswer('ru', 'greeting.hi', 'Здравствуйте. Как я могу к вам обращаться?'); 
// задавать этот вопрос если имя отсутствует в контексте

// manager.addNamedEntityText('name', 'Вася', ['ru'], ['вася', 'Вася', 'васек', 'Василий']);
// manager.addNamedEntityText('time', 'Утро', ['ru'], ['в первой половине дня', 'утром']);
// manager.addNamedEntityText('time', 'День', ['ru'], ['днем', 'днём', 'после 12']);
// manager.addNamedEntityText('time', 'Вечер', ['ru'], ['вечером', 'после 6']);

// manager.addDocument('ru', 'Меня зовут %name%', 'greeting.name');
// manager.addDocument('ru', 'Мое имя %name%', 'greeting.name');
//     manager.addAnswer('ru', 'greeting.name', '{{name}}, очень приятно с вами познакомиться. Какой у вас вопрос?');
//     manager.addAnswer('ru', 'greeting.name', 'Приятно познакомится, {{name}}. Какой у вас вопрос?');
//     manager.addAnswer('ru', 'greeting.name', 'Здравствуйте, {{name}}. Какой у вас вопрос?');

// manager.addDocument('ru', 'Какая стоимость карты', 'card.price');
// manager.addDocument('ru', 'Подскажите цену', 'card.price');
// manager.addDocument('ru', 'Сколько стоит карта', 'card.price');
// manager.addAnswer('ru', 'card.price', 'У нас более 60 клубных карт. Подскажите, какая конкретно область вас интересует?');

// manager.addDocument('ru', 'мне нужен бассейн', 'card.swim.price');
// manager.addDocument('ru', 'бассейн', 'card.swim.price');
manager.addDocument('ru', 'бассейн', 'swim_slot');
    // manager.addAnswer('ru', 'card.swim.price', 'Вы хотите заниматься с тренером?');

// manager.addDocument('ru', 'мне нужна качалка', 'card.gym.price');
// manager.addDocument('ru', 'качалка', 'card.gym.price');
//     manager.addAnswer('ru', 'card.gym.price', 'Посещение качалки бесплатное');

toEntity = manager.addTrimEntity('swim_trainer', 'trim');
// toEntity.addBetweenCondition('ru', 'да', 'нет', { skip: ['swim_slot'] });
manager.addTrimEntity('swim_time', 'trim');
manager.addTrimEntity('swim_demo', 'trim');

// manager.addNamedEntityText('swim_trainer', 'да', ['ru'], ['Да', 'был']);
// manager.addNamedEntityText('swim_trainer', 'нет', ['ru'], ['Нет', 'не был']);

// manager.addNamedEntityText('swim_time', 'утро', ['ru'], ['утром', 'утро', 'в первой половине дня']);
// manager.addNamedEntityText('swim_time', 'день', ['ru'], ['днем', 'день', 'днём']);
// manager.addNamedEntityText('swim_time', 'вечер', ['ru'], ['вечером', 'вечер', 'во второй половине дня']);
// manager.addNamedEntityText('swim_time', 'да', ['ru'], ['да']);
// manager.addNamedEntityText('swim_time', 'нет', ['ru'], ['нет']);

// manager.addNamedEntityText('swim_demo', 'посетил', ['ru'], ['Да', 'был']);
// manager.addNamedEntityText('swim_demo', 'непосетил', ['ru'], ['Нет', 'не был']);


manager.slotManager.addSlot('swim_slot', 'swim_trainer', true, { ru: 'Вы хотите заниматься с тренером?' });
manager.slotManager.addSlot('swim_slot', 'swim_time', true, { ru: 'В какое время Вы хотите заниматься?' });
manager.slotManager.addSlot('swim_slot', 'swim_demo', true, { ru: 'Вы уже были в нашем бассейне?'});
manager.addAnswer('ru', 'swim_slot', 'слот закончен');

 // FIXME: проблема условий: слот просто ведёт по линии, без возможности переключиться на другой слот
 // нужно выяснить каким образом можно сделать переход с одного слота на другой





// manager.addDocument('ru', 'нет', 'card.swim.self');
// manager.addDocument('ru', 'нет, я буду плавать один', 'card.swim.self');
// manager.addDocument('ru', 'нет, я буду плавать сам', 'card.swim.self');
// manager.addDocument('ru', 'нет, мне не нужен тренер', 'card.swim.self');
//     manager.addAnswer('ru', 'card.swim.self', 'Хорошо. А в какое время вы планируете ходить заниматься?');

// manager.addDocument('ru', 'я планирую заниматься %time%', 'card.swim.self.time');
// manager.addDocument('ru', 'я буду ходить %time%', 'card.swim.self.time');
// manager.addDocument('ru', 'в %time%', 'card.swim.self.time');
//     manager.addAnswer('ru', 'card.swim.self', 'Хорошо. Вы уже были в нашем бассейне?');

// manager.addDocument('ru', 'да, был', 'card.swim.self.visited');
// manager.addDocument('ru', 'да', 'card.swim.self.visited');
// manager.addDocument('ru', 'мне нужна стоимость', 'card.swim.self.visited');
// manager.addDocument('ru', 'мне нужна только стоимость', 'card.swim.self.visited');
// manager.addDocument('ru', 'не был', 'card.swim.self.notvisited');
// manager.addDocument('ru', 'нет', 'card.swim.self.notvisited'); // FIXME:
//     manager.addAnswer('ru', 'card.swim.self.visited', 'Ок. Стоимость занятия от 20000 долларов');
//     manager.addAnswer('ru', 'card.swim.self.notvisited', 'Тогда приглашаю вас на бесплатное пробное занятие');


// TODO: проверка наличия данных в контексте?





// manager.addDocument('ru', 'Сколько стоит ', 'type.answer');
// manager.addDocument('ru', 'стоимость занятия в бассейне', 'swim.price');
    // manager.addAnswer('ru', 'type.answer', 'answer handler');

// manager.addDocument('ru', 'Меня интересует %card_type%', 'card.type');
//     manager.addAnswer('ru', 'card.type', 'card.type handler');


// manager.addDocument('ru', 'Какая стоимость посещения бассейна', 'swim.price');
// manager.addDocument('ru', 'Подскажите цену бассейна', 'swim.price');
// manager.addDocument('ru', 'Сколько стоит занятие в бассейне', 'swim.price');


// Train also the NLG
// manager.addAnswer('ru', 'card.price', 'price anser');
// manager.addAnswer('ru', 'user.name', 'Вас зовут {{name}}');



module.exports = manager;
