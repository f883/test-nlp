
/*
// Train and save the model.
(async () => {
  await manager.train();
  manager.save();
  const response = await manager.process('ru', 'Привет');
  console.log(response);
})();
*/
const {NlpManager} = require('node-nlp');

/**
 * Train our model
 */
const trainModel = async (manager, filename) => {
  await manager.train();
  manager.save(`./models/${filename}`);
};

const loadModel = (filename, languages = ['ru']) => {
  const manager = new NlpManager({languages});
  manager.load(`./models/${filename}`);
  return manager;
};

module.exports = {
  trainModel,
  loadModel,
};
